import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";
import { HttpService } from "./services/http.service";
import environment from './environment';

@autoinject
export class JwtReader {
  constructor(private readonly router: Router, private readonly http: HttpService) {
  }

  activate(params: any) {
    this.http.configure(true);

    if (environment.debug) {
      this.router.navigateToRoute('home', params, { replace: true });
    } else {
      this.router.navigateToRoute('home');
    }
  }
}
