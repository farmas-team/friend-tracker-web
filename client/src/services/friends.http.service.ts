import { autoinject } from 'aurelia-framework';
import { FriendEntity, Friend } from './models';
import { HttpService } from './http.service';
import * as moment from 'moment';

@autoinject
export class FriendsHttpService {
  constructor(private readonly http: HttpService) {
  }

  getFriends(): Promise<FriendEntity[]> {
    return this.http.get('/api/friends');
  }

  addFriend(newFriend: Friend): Promise<FriendEntity> {
    return this.http.post('/api/friends', newFriend);
  }

  addFriends(newFriends: Friend[]): Promise<FriendEntity[]> {
    return this.http.post('/api/friends/bulkAdd', newFriends);
  }

  deleteFriend(id: string): Promise<any> {
    return this.http.delete(`/api/friends/${id}`);
  }

  updateFriend(friend: FriendEntity): Promise<FriendEntity> {
    return this.http.put(`/api/friends/${friend._id}`, friend);
  }

  updateContactDate(id: string): Promise<FriendEntity> {
    return this.http.put(`/api/friends/${id}`, { contactDate: new Date().getTime() });
  }

  formatDate(date: number): string {
    return moment(date).format('MMMM Do YYYY');
  }
}
