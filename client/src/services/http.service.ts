import { HttpClient } from "aurelia-fetch-client";
import { autoinject } from "aurelia-framework";
import * as toastr from 'toastr';

@autoinject
export class HttpService {
  private jwt: string = null;

  constructor(private readonly http: HttpClient) {
    this.configure();
  }

  configure(force: boolean = false) {
    if (!!this.jwt && !force) {
      return;
    }

    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    const query = window.location.hash.split('=');
    const isJwt = query.length > 0 && query[0].endsWith('jwt');

    this.jwt = isJwt && query.length > 1 && query[1];

    if (this.jwt) {
      headers['Authorization'] = 'Bearer ' + this.jwt;
    }

    this.http.configure(config => {
      config.withDefaults({
        credentials: 'same-origin',
        headers
      });
      config.rejectErrorResponses();
    });
  }

  get(url: string): Promise<any> {
    return this.requestJson('GET', url);
  }

  post<T>(url: string, body: object): Promise<T> {
    return this.requestJson('POST', url, body);
  }

  delete(url: string): Promise<any> {
    return this.request('DELETE', url);
  }

  put<T>(url: string, body: object): Promise<T> {
    return this.requestJson('PUT', url, body);
  }

  requestJson(method: string, url: string, body: any = null): Promise<any> {
    return new Promise((resolve, reject) => {
      this.requestRaw(method, url, body)
        .then(result => result.json())
        .then(data => resolve(data))
        .catch((reason: Response) => this._handleError(reason, reject));
    });
  }

  request(method: string, url: string, body: any = null): Promise<any> {
    return new Promise((resolve, reject) => {
      this.requestRaw(method, url, body)
        .then(data => resolve(data))
        .catch((reason: Response) => this._handleError(reason, reject));
    });
  }

  requestRaw(method: string, url: string, body: any = null): Promise<any> {
    var requestObj = { method: method.toLocaleUpperCase() };

    if (body) {
      requestObj['body'] = JSON.stringify(body)
    }

    return this.http.fetch(url, requestObj);
  }

  private _handleError(reason: Response, reject: (reason?: any) => void) {
    let reasonText: string;

    let logError = () => {
      console.log(reasonText);
      toastr.error(reasonText);
      reject(reasonText)
    };

    switch (reason.status) {
      case 401:
        reasonText = 'Unauthorized. ';
        break;
      case 400:
        reasonText = 'Bad Request. ';
        break;
      default:
        reasonText = 'Unknown Error. ';
    }

    if (!reason.json) {
      reasonText += reason.toString();
      logError();
    } else {
      reason.json().then(reasonJson => {
        if (reasonJson.message && reasonJson.message.length > 0 && reasonJson.message[0].constraints) {
          reasonText += JSON.stringify(reasonJson.message[0].constraints);
        } else if (reasonJson.message) {
          reasonText += JSON.stringify(reasonJson.message);
        } else {
          reasonText += JSON.stringify(reasonJson);
        }
        logError();
      }).catch((error) => {
        reasonText += error;
        logError();
      });
    }
  }
}
