import { FriendEntity } from "../services/models";

export class FriendDeleted {
  constructor(public friend: FriendEntity) { }
}

export class FriendCreated {
  constructor(public friend: FriendEntity) { }
}

export class FriendsCreated {
  constructor(public friends: FriendEntity[]) { }
}

export class FriendUpdated {
  constructor(public friend: FriendEntity) { }
}
