import { autoinject } from "aurelia-framework";
import { User } from "../types";
import { HttpService } from "./http.service";

@autoinject
export class UsersHttpService {
  private userPromise: Promise<User>;

  constructor(private readonly http: HttpService) {
  }

  getUserInfo(): Promise<User> {
    if (!this.userPromise) {
      this.userPromise = this.http.requestRaw('GET', '/auth/userInfo')
        .then(result => result.json())
        .then(user => { return { ...user, isAuthenticated: true } })
        .catch(() => {
          return { isAuthenticated: false };
        });
    }

    return this.userPromise;
  }
}
