import { autoinject } from "aurelia-framework";
import { HttpService } from "./http.service";
import { PagedContacts } from "../services/models";

@autoinject
export class ContactsHttpService {
  constructor(private readonly http: HttpService) {
  }

  getContacts(pageToken: string = ''): Promise<PagedContacts> {
    return this.http.get(`/api/contacts?pageToken=${pageToken}`);
  }
}
