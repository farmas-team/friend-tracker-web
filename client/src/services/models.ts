export interface Entity {
  _id?: string;
}

export interface Friend {
  providerId?: string;
  name: string;
  email: string;
  imageUrl: string;
  contactDate: number;
}

export interface FriendEntity extends Friend, Entity {
}

export interface PagedContacts {
  nextPageToken: string;
  totalContacts: number;
  contacts: Friend[];
}
