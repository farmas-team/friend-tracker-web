import { Router, RouterConfiguration } from 'aurelia-router';
import { autoinject, PLATFORM } from 'aurelia-framework';

@autoinject
export class App {
  public router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.map([
      {
        route: ['', 'home'],
        name: 'home',
        moduleId: PLATFORM.moduleName('./components/home/home'),
        nav: false,
        title: 'Home'
      },
      {
        route: 'loggedin',
        name: 'loggedin',
        moduleId: PLATFORM.moduleName('./jwt-reader'),
      }
    ]);

    this.router = router;
  }
}
