import { autoinject } from "aurelia-framework";
import { DialogController } from "aurelia-dialog";
import { EventAggregator } from "aurelia-event-aggregator";
import { Validator, ValidationControllerFactory, ValidationController, validateTrigger, ValidationRules } from "aurelia-validation";
import { FriendsHttpService } from "../../services/friends.http.service";
import { Friend, FriendEntity } from "../../services/models";
import { FriendUpdated } from "../../services/events";
import * as moment from "moment";

export interface EditFriendDialogModel {
  friend: FriendEntity;
}

interface EditableFriend extends FriendEntity {
  dateStr: string;
}

@autoinject
export class EditFriendDialog {
  public friend: EditableFriend;
  public canSave = false;
  public validationController: ValidationController;

  private originalDateStr: string;

  constructor(
    public readonly dialogController: DialogController,
    private readonly friendService: FriendsHttpService,
    private readonly messages: EventAggregator,
    private readonly validator: Validator,
    controllerFactory: ValidationControllerFactory) {

    this.validationController = controllerFactory.createForCurrentScope(validator);
    this.validationController.validateTrigger = validateTrigger.changeOrBlur;
    this.validationController.subscribe(() => this.validate());

    this.validationController = controllerFactory.createForCurrentScope();
  }

  activate(model: EditFriendDialogModel) {
    this.originalDateStr = moment(model.friend.contactDate).format('YYYY-MM-DD');

    this.friend = {
      ...model.friend,
      dateStr: this.originalDateStr
    };

    ValidationRules
      .ensure((p: Friend) => p.name).displayName('Name').required()
      .ensure((p: Friend) => p.email).displayName('Email').email()
      .on(this.friend);
  }

  editFriend(): Promise<any> {
    if (this.originalDateStr !== this.friend.dateStr) {
      this.friend.contactDate = moment(this.friend.dateStr, 'YYYY-MM-DD').toDate().getTime();
    }

    return this.friendService.updateFriend(this.friend).then((friend: FriendEntity) => {
      this.messages.publish(new FriendUpdated(friend));
      this.dialogController.close(true, friend);
    });
  }

  private validate() {
    this.validator.validateObject(this.friend)
      .then(results => this.canSave = results.every(result => result.valid));
  }
}
