import { autoinject } from "aurelia-framework";
import { DialogController } from "aurelia-dialog";

export interface ConfirmDialogModel {
  text: string;
}

@autoinject
export class ConfirmDialog {
  public text: string;

  constructor(public readonly controller: DialogController) {
  }

  activate(model: ConfirmDialogModel) {
    this.text = model.text;
  }
}
