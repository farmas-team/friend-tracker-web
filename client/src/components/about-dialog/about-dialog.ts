import { autoinject } from "aurelia-framework";
import { DialogController } from "aurelia-dialog";

@autoinject
export class AboutDialog {
  constructor(public readonly controller: DialogController) {
  }
}
