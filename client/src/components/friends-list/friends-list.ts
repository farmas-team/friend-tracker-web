import { autoinject } from 'aurelia-framework';
import { DialogService } from 'aurelia-dialog';
import { FriendsHttpService } from '../../services/friends.http.service';
import { FriendEntity } from '../../services/models';
import { EventAggregator } from 'aurelia-event-aggregator';
import { FriendDeleted, FriendCreated, FriendsCreated, FriendUpdated } from '../../services/events';
import { AddFriendDialog } from '../add-friend-dialog/add-friend-dialog';
import { ImportFriendsDialog, ImportFriendsDialogModel } from '../import-friends-dialog/import-friends-dialog';
import { FriendCardModel } from '../friend-card/friend-card';
import * as moment from 'moment';

const SORT_NAME = 'Name';
const SORT_DATE = 'Contact Date';

@autoinject
export class FriendsList {
  public loading = true;
  public friends: FriendCardModel[] = [];
  public sortOrders: string[] = [SORT_NAME, SORT_DATE];
  public selectedSortOrder: string = SORT_NAME;
  public isGroupEnabled = true;
  public coldFriends: FriendCardModel[] = [];
  public warmFriends: FriendCardModel[] = [];

  private showingDialog: boolean = false;

  constructor(
    private readonly friendService: FriendsHttpService,
    private readonly messages: EventAggregator,
    private readonly dialogService: DialogService) {
  }

  created() {
    this.messages.subscribe(FriendDeleted, (evt: FriendDeleted) => this.handleFriendDeleted(evt.friend));
    this.messages.subscribe(FriendCreated, (evt: FriendCreated) => this.handleFriendCreated(evt.friend));
    this.messages.subscribe(FriendsCreated, (evt: FriendsCreated) => this.handleFriendsCreated(evt.friends));
    this.messages.subscribe(FriendUpdated, (evt: FriendUpdated) => this.handleFriendUpdated(evt.friend));

    this.friendService.getFriends().then(data => {
      this.friends = data.map<FriendCardModel>(friend => {
        return {
          ...friend,
          lastContactDateStr: this.friendService.formatDate(friend.contactDate)
        }
      });

      this.sortFriends();
      this.loading = false;
    });
  }

  showCreateFriendDialog(): void {
    if (!this.showingDialog) {
      this.showingDialog = true;
      this.dialogService.open({ viewModel: AddFriendDialog }).whenClosed(() => this.showingDialog = false);
    }
  }

  showImportFriendsDialog(): void {
    if (!this.showingDialog) {
      this.showingDialog = true;

      const preSelectedFriends = this.friends.filter(f => !!f.providerId).map(f => f.providerId);
      const model: ImportFriendsDialogModel = { preSelectedFriendIds: preSelectedFriends };
      this.dialogService.open({ viewModel: ImportFriendsDialog, model, centerHorizontalOnly: true }).whenClosed(() => this.showingDialog = false);
    }
  }

  sortOrderChanged(): void {
    this.sortFriends();
  }

  handleFriendCreated(friend: FriendEntity) {
    this.friends.push({ ...friend, lastContactDateStr: this.friendService.formatDate(friend.contactDate) });
    this.sortFriends();
  }

  handleFriendsCreated(friends: FriendEntity[]) {
    friends.forEach(friend => this.friends.push({ ...friend, lastContactDateStr: this.friendService.formatDate(friend.contactDate) }));
    this.sortFriends();
  }

  handleFriendDeleted(friend: FriendEntity) {
    const index = this.friends.indexOf(friend);

    if (index >= 0) {
      this.friends.splice(index, 1);
    }

    this.sortFriends();
  }

  handleFriendUpdated(friend: FriendEntity) {
    this.sortFriends();
  }

  private sortFriends(): void {
    if (!this.isGroupEnabled) {
      this.sortFriendArray(this.friends);
    } else {
      const lastYear = moment().subtract(11, 'months');
      this.warmFriends.splice(0, this.warmFriends.length);
      this.coldFriends.splice(0, this.coldFriends.length);

      this.friends.forEach(friend => {
        const date = moment(friend.contactDate);
        if (date.diff(lastYear) < 0) {
          this.coldFriends.push(friend);
        } else {
          this.warmFriends.push(friend);
        }
      });

      this.sortFriendArray(this.warmFriends);
      this.sortFriendArray(this.coldFriends);
    }
  }

  private sortFriendArray(friends: FriendCardModel[]): void {
    friends.sort((a, b) => {
      if (this.selectedSortOrder == SORT_NAME) {
        const name1 = a.name || '';
        const name2 = b.name || '';
        return name1.localeCompare(name2);
      } else {
        const date1 = moment(a.contactDate);
        const date2 = moment(b.contactDate);
        return date1.diff(date2);
      }
    });
  }
}
