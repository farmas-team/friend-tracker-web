import { bindable } from "aurelia-framework";
import { DialogController } from "aurelia-dialog";

export class ModalDialog {
  isBusy: boolean = false;
  @bindable okButtonText: string = 'Ok';
  @bindable title: string;
  @bindable controller: DialogController;
  @bindable callback: Function;
  @bindable okButtonEnabled: boolean = true;
  @bindable submitOnEnter: boolean = true;
  @bindable showCancelButton: boolean = true;

  bind() {
    window.addEventListener('keydown', this.handleKeyInput, false);

    if (!this.controller) {
      throw new Error('Controller is required');
    }

    if (!this.callback) {
      this.callback = () => {
        this.controller.close(true);
        return null;
      };
    }
  }

  unbind() {
    window.removeEventListener('keydown', this.handleKeyInput);
  }

  handleKeyInput = (event: any) => {
    if (this.isBusy || !this.controller) {
      return;
    }

    event = event || window.event;
    if (event.keyCode == 27) {
      this.controller.cancel();
    } else if (event.keyCode == 13 && this.okButtonEnabled && this.submitOnEnter) {
      this.isBusy = true;

      let promise: Promise<any> = this.callback();
      if (!!promise) {
        // if the callback returns a rejected promise, the buttons should become enabled again.
        promise.then(null, () => this.isBusy = false);
      }

      event.preventDefault();
      event.stopPropagation();
    }
  }
}
