import { autoinject } from "aurelia-framework";
import { ValidationRules, ValidationControllerFactory, ValidationController, Validator, validateTrigger } from "aurelia-validation";
import { DialogController } from "aurelia-dialog";
import { FriendsHttpService } from "../../services/friends.http.service";
import { EventAggregator } from "aurelia-event-aggregator";
import { FriendCreated } from "../../services/events";
import { FriendEntity, Friend } from "../../services/models";

@autoinject
export class AddFriendDialog {
  public canSave = false;
  public friend: Friend = { name: '', email: '', imageUrl: '', contactDate: new Date().getTime() };
  public validationController: ValidationController;

  constructor(
    public readonly dialogController: DialogController,
    private readonly friendService: FriendsHttpService,
    private readonly messages: EventAggregator,
    private readonly validator: Validator,
    controllerFactory: ValidationControllerFactory) {

    this.validationController = controllerFactory.createForCurrentScope(validator);
    this.validationController.validateTrigger = validateTrigger.changeOrBlur;
    this.validationController.subscribe(() => this.validate());

    this.validationController = controllerFactory.createForCurrentScope();
  }

  activate() {
    ValidationRules
      .ensure((p: Friend) => p.name).displayName('Name').required()
      .ensure((p: Friend) => p.email).displayName('Email').email()
      .on(this.friend);
  }

  addFriend() {
    return this.friendService.addFriend(this.friend).then((friend: FriendEntity) => {
      this.messages.publish(new FriendCreated(friend));
      this.dialogController.close(true, friend);
    });
  }

  private validate() {
    this.validator.validateObject(this.friend)
      .then(results => this.canSave = results.every(result => result.valid));
  }
}
