import { autoinject, bindable } from "aurelia-framework";
import { EventAggregator } from "aurelia-event-aggregator";
import { DialogService } from "aurelia-dialog";
import { FriendEntity } from "../../services/models";
import { FriendDeleted, FriendUpdated } from "../../services/events";
import { FriendsHttpService } from "../../services/friends.http.service";
import { ConfirmDialog, ConfirmDialogModel } from "../confirm-dialog/confirm-dialog";
import { EditFriendDialogModel, EditFriendDialog } from "../edit-friend-dialog/edit-friend-dialog";

export interface FriendCardModel extends FriendEntity {
  isSelected?: boolean;
  isLocked?: boolean;
  isSelectable?: boolean;
  lastContactDateStr?: string;
}

// TODO: Update the imageurl to add rounded borders.
@autoinject
export class FriendCard {
  @bindable friend: FriendCardModel;
  @bindable showFooter: boolean = true;
  @bindable showEmailLink: boolean = true;
  @bindable showContactDate: boolean = true;

  constructor(
    private readonly messages: EventAggregator,
    private readonly friendService: FriendsHttpService,
    private readonly dialogService: DialogService) {
  }

  created() {
    this.messages.subscribe(FriendUpdated, (evt: FriendUpdated) => this.handleFriendUpdated(evt.friend));
  }

  delete(): void {
    let model: ConfirmDialogModel = { text: `Are you sure you want to delete '${this.friend.name}?'` };

    this.dialogService.open({ viewModel: ConfirmDialog, model: model }).whenClosed(response => {
      if (!response.wasCancelled) {
        this.friendService.deleteFriend(this.friend._id).then(() => {
          this.messages.publish(new FriendDeleted(this.friend));
        });
      }
    });
  }

  edit(): void {
    let model: EditFriendDialogModel = { friend: this.friend };

    this.dialogService.open({ viewModel: EditFriendDialog, model: model });
  }

  updateDate(): void {
    this.friendService.updateContactDate(this.friend._id).then((friend) => {
      this.friend.contactDate = friend.contactDate;
      this.friend.lastContactDateStr = this.friendService.formatDate(friend.contactDate);
      this.messages.publish(new FriendUpdated(this.friend));
    });
  }

  clicked(): void {
    if (this.friend.isSelectable && !this.friend.isLocked) {
      this.friend.isSelected = !this.friend.isSelected;
    }
  }

  handleFriendUpdated(friend: FriendEntity) {
    if (friend._id === this.friend._id) {
      this.friend.contactDate = friend.contactDate;
      this.friend.name = friend.name;
      this.friend.email = friend.email;
      this.friend.imageUrl = friend.imageUrl;
      this.friend.lastContactDateStr = this.friendService.formatDate(friend.contactDate);
    }
  }
}
