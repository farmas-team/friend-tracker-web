import { autoinject } from "aurelia-framework";
import { DialogController } from "aurelia-dialog";
import { ContactsHttpService } from "../../services/contacts.http.service";
import { PagedContacts, FriendEntity } from "../../services/models";
import { FriendCardModel } from "../friend-card/friend-card";
import { EventAggregator } from "aurelia-event-aggregator";
import { FriendsCreated } from "../../services/events";
import { FriendsHttpService } from "../../services/friends.http.service";

export interface ImportFriendsDialogModel {
  preSelectedFriendIds: string[];
}

interface ContactPage {
  contacts: FriendCardModel[];
  pageToken?: string;
}

@autoinject
export class ImportFriendsDialog {
  private preSelectedIds: Record<string, boolean> = {};
  private currentPageNumber: number = 1;
  private totalPages: number = 1;
  private pageSize = 20;
  private pages: Record<number, ContactPage> = {};

  public loading: boolean = true;
  public contacts: FriendCardModel[] = [];

  constructor(
    public readonly dialogController: DialogController,
    private readonly friendService: FriendsHttpService,
    private readonly messages: EventAggregator,
    private readonly contactService: ContactsHttpService) {
  }

  activate(model: ImportFriendsDialogModel) {
    if (model && model.preSelectedFriendIds) {
      model.preSelectedFriendIds.forEach(id => this.preSelectedIds[id] = true);
    }

    this.loadContactPage(1);
  }

  get canSave(): boolean {
    return !this.loading;
  }

  get subTitle(): string {
    return `Page ${this.currentPageNumber} of ${this.totalPages}`;
  }

  get isPreviousEnabled(): boolean {
    return this.currentPageNumber > 1;
  }

  previousPage(): void {
    this.loadContactPage(this.currentPageNumber - 1);
  }

  get isNextEnabled(): boolean {
    return this.currentPageNumber < this.totalPages;
  }

  nextPage(): void {
    this.loadContactPage(this.currentPageNumber + 1);
  }

  firstPage(): void {
    this.loadContactPage(1);
  }

  addContacts() {
    this.loading = true;

    const friends = Object.keys(this.pages)
      .map<ContactPage>(key => this.pages[key])
      .reduce<FriendCardModel[]>((arr, page) => arr.concat(page.contacts), [])
      .filter(c => c.isSelected);

    return this.friendService.addFriends(friends)
      .then((friends: FriendEntity[]) => {
        this.messages.publish(new FriendsCreated(friends));
        this.dialogController.close(true, friends);
      })
      .catch(() => this.loading = false);
  }

  private loadContactPage(pageNumber: number): void {
    const newPage = this.pages[pageNumber];
    const newPageToken = newPage && newPage.pageToken;
    let newPageContacts = newPage && newPage.contacts;

    if (newPageContacts && newPageContacts.length > 0) {
      this.contacts = newPageContacts;
      this.currentPageNumber = pageNumber;
    } else {
      this.loading = true;

      this.contactService.getContacts(newPageToken).then((result: PagedContacts) => {
        newPageContacts = result.contacts.map<FriendCardModel>(friend => {
          const isLocked = !!this.preSelectedIds[friend.providerId || ''];
          return {
            ...friend,
            isLocked,
            isSelected: false,
            isSelectable: !isLocked
          };
        });

        this.contacts = newPageContacts;
        this.pages[pageNumber] = { contacts: newPageContacts };
        this.currentPageNumber = pageNumber;
        this.totalPages = Math.ceil(result.totalContacts / this.pageSize);

        if (result.nextPageToken) {
          // prepare the next page.
          this.pages[pageNumber + 1] = { contacts: [], pageToken: result.nextPageToken };
        }

        this.loading = false;
      });
    }
  }
}
