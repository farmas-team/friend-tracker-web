import { autoinject } from 'aurelia-framework';
import { UsersHttpService } from "../../services/users.http.service";
import { User } from "../../types";

// TODO: Update the anonymous view to include a proper formatted message.
@autoinject
export class Home {
  public user: User;

  constructor(private readonly userService: UsersHttpService) {
  }

  created() {
    this.userService.getUserInfo().then(user => this.user = user);
  }
}
