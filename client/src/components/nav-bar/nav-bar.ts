import { autoinject } from "aurelia-framework";
import { DialogService } from "aurelia-dialog";
import { UsersHttpService } from "../../services/users.http.service";
import { User } from "../../types";
import { AboutDialog } from "../about-dialog/about-dialog";

@autoinject
export class NavBar {
  public user: User;

  constructor(private readonly userService: UsersHttpService, private readonly dialogService: DialogService) {
  }

  created() {
    this.userService.getUserInfo().then(user => this.user = user);
  }

  showAboutDialog() {
    this.dialogService.open({ viewModel: AboutDialog });
  }
}
