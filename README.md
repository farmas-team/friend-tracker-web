
# Friends Tracker

Application that helps to remind you to contact your old friends and collegues.

Sample site: https://myfriendstracker.herokuapp.com/

# Architecture

Back-end server is written using [Nest.js](https://nestjs.com) and front-end is built with [Aurelia](https://aurelia.io). The root folder of the project is the back-end server, the aurelia application is contained whithin the 'client' folder.

## Development
During development both the back-end and the front-end dev servers are run concurrently to enable each of the frameworks hot-reload mechanisms to work. The Aurelia dev server proxies all API calls to the Nest dev server.

## Deployment
During deployment, the bundled aurelia application is served by the Nest server using a static file middle-ware.

## Authentication
Authentication is performed using Google OAuth provider and authorization is done with JWT bearer tokens.

## Database
Application uses [MongoDB](https://www.mongodb.com/) to store data and [Typegoose](https://github.com/szokodiakos/typegoose) for the data access layer on the server. 

# Setup for Development

## Requirements

- Docker (to host mongodb)
- Node 10

## Back-End (root folder)

- Make sure you have created an application in the [Google Developer Console](https://console.developers.google.com), setup an OAuth credential for the application and gather your client id and client secret.

- If you want to enable the feature of importing contacts from Google, make sure to enable the 'Contacts API' and the 'People API' on the Google Developers Console.

- Start mongodb inside a Docker container

```bash
docker run --rm -p 27017:27017 --name friendtracker-db mongo
```

- Install dependencies of back-end.
```bash
$ npm install
```

- Create a new signature key for JWT
```bash
node -e "console.log(require('crypto').randomBytes(256).toString('base64'));"
```

- Setup environment variables. Create a '.env' file on the root directory with the following contents:
```
GOOGLE_CLIENT_ID = <your_client_id>
GOOGLE_CLIENT_SECRET = <your_client_secret>
JWT_SECRET = <your_jwt_signing_key>
```

- Start back-end dev server
```bash
$ npm run start:dev
```

## Front-End ('client' folder)

- Install dependencies of front-end.
```bash
$ npm install
```

-  Start front-end dev server
```bash
$ au run
```

- Navigate to http://localhost:8080 to load application.

# Deployment

## Heroku
- Create an application in [heroku](https://www.heroku.com).
- Install the heroku-cli on your machine.
```bash
$ heroku login
$ heroku git:remote -a MY-HEROKU-APP
$ heroku config:set GOOGLE_CLIENT_ID=<your_client_id>
$ heroku config:set GOOGLE_CLIENT_SECRET=<your_client_secret>
$ heroku config:set JWT_SECRET=<your_jwt_signing_key>
$ heroku config:set HOST_URL=<your_heroku_app_url>
$ heroku config:set MONGODB_URL=<your_mongodb_url>
$ git push heroku master
```

# Serverless Back-End

The second part of this project is a serverless function that will email regular reminders to the users to contact their friends. This project in available in [this repo](https://bitbucket.org/farmas-team/friend-tracker-functions).