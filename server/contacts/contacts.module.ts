import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { ContactsController } from './contacts.controller';
import { ContactsService } from './contacts.service';
import { UserService } from '../auth/user.service';
import { UserRequestService } from '../auth/user.request.service';

@Module({
    controllers: [ContactsController],
    imports: [AuthModule],
    providers: [ContactsService, UserService, UserRequestService],
})
export class ContactsModule { }
