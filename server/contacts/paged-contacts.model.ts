import { Contact } from './contact.model';

export class PagedContacts {
    nextPageToken: string;
    totalContacts: number;
    contacts: Contact[];
}
