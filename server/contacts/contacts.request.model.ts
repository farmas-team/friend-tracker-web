import { IsOptional, IsString } from 'class-validator';

export class GetContactsRequestModel {
    @IsString()
    @IsOptional()
    readonly pageToken: string;
}
