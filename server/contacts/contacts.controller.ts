import { Controller, Get, UseGuards, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ContactsService } from './contacts.service';
import { GetContactsRequestModel } from './contacts.request.model';

@Controller('api/contacts')
export class ContactsController {
    constructor(private readonly contactService: ContactsService) {
    }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    async getContacts(@Query() options: GetContactsRequestModel): Promise<any> {
        return this.contactService.getContacts(options.pageToken);
    }
}
