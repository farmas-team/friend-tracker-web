export class Contact {
    providerId: string;
    name: string;
    email?: string;
    imageUrl?: string;
}