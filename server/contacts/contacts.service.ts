import { Injectable } from '@nestjs/common';
import { google, people_v1 } from 'googleapis';
import { UserService } from '../auth/user.service';
import { UserRequestService } from '../auth/user.request.service';
import { Contact } from './contact.model';
import { PagedContacts } from './paged-contacts.model';

@Injectable()
export class ContactsService {
    constructor(private readonly userService: UserService, private readonly userReqService: UserRequestService) {
    }

    // TODO: Better error handling before sending the request.
    async getContacts(pageToken: string = null, pageSize: number = 20): Promise<any> {
        const userId = this.userReqService.getLoggedInUserId();
        const user = await this.userService.findById(userId);
        const contacts = google.people({ version: 'v1' });

        return new Promise((resolve, reject) => {
            contacts.people.connections.list({
                resourceName: 'people/me',
                personFields: 'names,emailAddresses,photos',
                access_token: user.accessToken,
                sortOrder: 'FIRST_NAME_ASCENDING',
                pageSize,
                pageToken,
            }, (err, response) => {
                if (err) {
                    reject(err.message);
                } else {
                    const pagedContacts: PagedContacts = {
                        contacts: [],
                        totalContacts: 0,
                        nextPageToken: null,
                    };

                    if (response && response.data && response.data.connections) {
                        pagedContacts.totalContacts = response.data.totalItems;
                        pagedContacts.nextPageToken = response.data.nextPageToken;

                        const contactsList = response.data.connections.map(c => this.parseContact(c));
                        pagedContacts.contacts = contactsList.filter(c => c.name || c.email);
                    }

                    resolve(pagedContacts);
                }
            });
        });
    }

    private parseContact(person: people_v1.Schema$Person): Contact {
        const contact = new Contact();

        contact.providerId = person.resourceName;

        if (person.names && person.names.length > 0) {
            contact.name = person.names[0].displayName;
        }

        if (person.photos && person.photos.length > 0) {
            contact.imageUrl = person.photos[0].url;
        }

        if (person.emailAddresses && person.emailAddresses.length > 0) {
            contact.email = person.emailAddresses[0].value;
        }

        return contact;
    }
}
