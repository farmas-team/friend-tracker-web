import { Controller, Get, UseGuards, Post, Body, Param, Delete, Put } from '@nestjs/common';
import { FriendsService } from './friends.service';
import { Friend } from './friend.model';
import { AuthGuard } from '@nestjs/passport';
import { CreateFriendRequestModel, UpdateFriendRequestModel } from './friend.request.model';

@Controller('api/friends')
export class FriendsController {
    constructor(private readonly friendService: FriendsService) {
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    public async create(@Body() createFriend: CreateFriendRequestModel): Promise<Friend> {
        return await this.friendService.add(createFriend);
    }

    @Post('bulkAdd')
    @UseGuards(AuthGuard('jwt'))
    public async createBulk(@Body() createFriends: CreateFriendRequestModel[]): Promise<Friend[]> {
        return await this.friendService.addBulk(createFriends);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    public async update(@Param('id') id, @Body() updateFriend: UpdateFriendRequestModel): Promise<Friend> {
        return await this.friendService.update(id, updateFriend);
    }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    async getFriends(): Promise<Friend[]> {
        return await this.friendService.findAll();
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    async delete(@Param('id') id): Promise<any> {
        return await this.friendService.delete(id);
    }
}
