import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { Friend } from './friend.model';
import { ModelType } from 'typegoose';
import { url as gravatarUrl } from 'gravatar';
import { CreateFriendRequestModel, UpdateFriendRequestModel } from './friend.request.model';
import { UserRequestService } from '../auth/user.request.service';

const DefaultImageUrl = 'https://res.cloudinary.com/farmas/image/upload/v1558634451/friendtracker/friendship-128.png';

@Injectable()
export class FriendsService {
    constructor(
        private readonly userService: UserRequestService,
        @InjectModel(Friend) private readonly friendModel: ModelType<Friend>) {
    }

    async add(createFriend: CreateFriendRequestModel): Promise<Friend> {
        const isDuplicate = await this.isDuplicateEmailAsync(createFriend.email);

        if (isDuplicate) {
            throw new HttpException(`Friend with email '${createFriend.email}' already exists.`, HttpStatus.BAD_REQUEST);
        }

        if (!createFriend.imageUrl) {
            createFriend.imageUrl = this.buildImageUrl(createFriend.email);
        }

        const createdFriend = new this.friendModel({ ...createFriend, userId: this.userId, contactDate: this.getLastContactDate() });
        return createdFriend.save();
    }

    // TODO: Add guard against adding duplicate friends.
    async addBulk(createFriends: CreateFriendRequestModel[]): Promise<Friend[]> {
        if (!createFriends || createFriends.length === 0) {
            return [];
        }

        const friends = createFriends.map(friend => {
            const imageUrl = friend.imageUrl ? friend.imageUrl : this.buildImageUrl(friend.email);
            const name = friend.name || friend.email;
            return { ...friend, name, imageUrl, userId: this.userId, contactDate: this.getLastContactDate() };
        });

        return this.friendModel.insertMany(friends);
    }

    async findOne(query: object): Promise<Friend> {
        return this.friendModel.findOne(query).exec();
    }

    async findAll(): Promise<Friend[]> {
        return this.friendModel.find({ userId: this.userId }).exec();
    }

    async delete(id: string): Promise<any> {
        return this.friendModel.deleteOne({ _id: id, userId: this.userId }).exec();
    }

    async update(id: string, updateFriend: UpdateFriendRequestModel): Promise<Friend> {
        return this.friendModel.findOneAndUpdate({ _id: id, userId: this.userId }, updateFriend, { new: true });
    }

    private async isDuplicateEmailAsync(email: string): Promise<boolean> {
        if (email) {
            const oldFriend = await this.findOne({ email, userId: this.userId });
            return !!oldFriend;
        }

        return false;
    }

    private get userId(): string {
        return this.userService.getLoggedInUserId();
    }

    private buildImageUrl(email: string): string {
        if (email) {
            return gravatarUrl(email, {
                protocol: 'https',
                size: '80',
                default: DefaultImageUrl,
            });
        } else {
            return DefaultImageUrl;
        }
    }

    private getLastContactDate(): number {
        const date = new Date();
        date.setFullYear(date.getFullYear() - 1);
        return date.getTime();
    }
}
