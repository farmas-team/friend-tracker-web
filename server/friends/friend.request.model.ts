import { IsString, IsNotEmpty, IsEmail, IsOptional, ValidateIf, IsNumber } from 'class-validator';

export class CreateFriendRequestModel {
    @IsString()
    @IsNotEmpty()
    readonly name: string;

    @IsEmail()
    @ValidateIf(o => o.email && o.email.length > 0)
    readonly email: string;

    @IsString()
    @IsOptional()
    imageUrl: string;

    @IsString()
    @IsOptional()
    providerId: string;
}

// tslint:disable-next-line: max-classes-per-file
export class UpdateFriendRequestModel {
    @IsString()
    @IsOptional()
    readonly name: string;

    @IsEmail()
    @ValidateIf(o => o.email && o.email.length > 0)
    readonly email: string;

    @IsString()
    @IsOptional()
    imageUrl: string;

    @IsNumber()
    @IsOptional()
    contactDate: number;
}
