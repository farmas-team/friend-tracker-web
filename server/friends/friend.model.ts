import { prop, Typegoose } from 'typegoose';
import { IsString, IsNumber } from 'class-validator';

export class Friend extends Typegoose {
    @IsString()
    @prop({ required: true })
    userId: string;

    @IsString()
    @prop({ required: true })
    name: string;

    @IsString()
    @prop()
    email: string;

    @IsString()
    @prop({ required: true })
    imageUrl: string;

    @IsString()
    @prop()
    providerId: string;

    @IsNumber()
    @prop({ required: true })
    contactDate: number;
}
