import { Module } from '@nestjs/common';
import { FriendsController } from './friends.controller';
import { ConfigModule } from '../config/config.module';
import { TypegooseModule } from 'nestjs-typegoose';
import { Friend } from './friend.model';
import { FriendsService } from './friends.service';
import { AuthModule } from '../auth/auth.module';

@Module({
    controllers: [FriendsController],
    imports: [
        ConfigModule,
        AuthModule,
        TypegooseModule.forFeature(Friend),
    ],
    providers: [FriendsService],
})
export class FriendsModule { }
