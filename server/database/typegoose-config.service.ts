import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { TypegooseOptionsFactory, TypegooseModuleOptions } from 'nestjs-typegoose';

// TODO: Remove this file in favor of configuring the session in the database.module.
@Injectable()
export class TypegooseConfigService implements TypegooseOptionsFactory {
    constructor(private readonly config: ConfigService) {
    }

    createTypegooseOptions(): Promise<TypegooseModuleOptions> | TypegooseModuleOptions {
        return {
            uri: this.config.get('MONGODB_URL'),
            useNewUrlParser: true,
        };
    }
}
