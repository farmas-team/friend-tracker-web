import { Module } from '@nestjs/common';
import { TypegooseConfigService } from './typegoose-config.service';
import { ConfigService } from '../config/config.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { ConfigModule } from '../config/config.module';

@Module({
    imports: [
        TypegooseModule.forRootAsync({
            imports: [ConfigModule],
            useClass: TypegooseConfigService,
            inject: [ConfigService],
        }),
    ],
})
export class DatabaseModule { }
