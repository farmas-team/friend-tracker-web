import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

@Injectable()
export class ConfigService {
    private readonly envConfig: { [key: string]: string };

    constructor() {
        const env = process.env.NODE_ENV;

        if (env === 'production' || env === 'staging') {
            this.envConfig = {
                GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
                GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
                JWT_SECRET: process.env.JWT_SECRET,
                HOST_URL: process.env.HOST_URL,
                MONGODB_URL: process.env.MONGODB_URL,
            };
        } else {
            this.envConfig = dotenv.parse(fs.readFileSync('.env'));
            this.envConfig.CLIENT_BASE_URL = this.envConfig.CLIENT_BASE_URL || 'http://localhost:8080';
            this.envConfig.HOST_URL = this.envConfig.HOST_URL || 'http://localhost:3000';
            this.envConfig.MONGODB_URL = this.envConfig.MONGODB_URL || 'mongodb://localhost:27017/friendtracker';
        }
    }

    get(key: string): string {
        return this.envConfig[key];
    }
}
