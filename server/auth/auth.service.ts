import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { sign } from 'jsonwebtoken';
import { User, IUser } from './user.model';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
    private jwtSecret: string;

    constructor(private readonly config: ConfigService, private readonly userService: UserService) {
        this.jwtSecret = config.get('JWT_SECRET');
    }

    async validateOAuthLogin(externalUser: IUser): Promise<string> {
        try {
            let user = await this.userService.findByProviderId(externalUser.providerId);

            if (!user) {
                user = await this.userService.create(externalUser);
            } else {
                await this.userService.updateAccessToken(externalUser.providerId, externalUser.accessToken);
            }

            const payload = { id: user._id };
            const jwt: string = sign(payload, this.jwtSecret, { expiresIn: 3600 });

            return jwt;
        } catch (err) {
            throw new InternalServerErrorException('validateOAuthLogin', err.message);
        }
    }
}
