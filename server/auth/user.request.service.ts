import { Injectable, Scope, Inject } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

@Injectable({ scope: Scope.REQUEST })
export class UserRequestService {

    constructor(@Inject(REQUEST) private readonly request: Request) {
    }

    public getLoggedInUserId(): string {
        return this.request.user.id;
    }
}
