import { prop, Typegoose } from 'typegoose';
import { IsString } from 'class-validator';
import { Schema } from 'mongoose';

export interface IUser {
    providerId: string;
    displayName: string;
    email: string;
    accessToken: string;
}

export class User extends Typegoose implements IUser {
    _id: Schema.Types.ObjectId;

    @IsString()
    @prop({ required: true })
    providerId: string;

    @IsString()
    @prop({ required: true })
    displayName: string;

    @IsString()
    @prop({ required: true })
    email: string;

    @IsString()
    @prop({ required: true })
    accessToken: string;
}
