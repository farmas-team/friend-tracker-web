import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ConfigModule } from '../config/config.module';
import { GoogleStrategy } from './google.strategy';
import { JwtStrategy } from './jwt.strategy';
import { UserService } from './user.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { User } from './user.model';
import { UserRequestService } from './user.request.service';

@Module({
  controllers: [AuthController],
  providers: [AuthService, UserService, UserRequestService, GoogleStrategy, JwtStrategy],
  imports: [
    ConfigModule,
    TypegooseModule.forFeature(User),
  ],
  exports: [UserRequestService, UserService],
})
export class AuthModule { }
