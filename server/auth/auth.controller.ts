import { Controller, Get, UseGuards, Req, Res } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ConfigService } from '../config/config.service';
import { UserService } from './user.service';
import { IUserResponseModel } from './user.response.model';

@Controller('auth')
export class AuthController {
    constructor(private readonly config: ConfigService, private readonly userService: UserService) {
    }

    @Get('userInfo')
    @UseGuards(AuthGuard('jwt'))
    public async userInfo(@Req() req): Promise<IUserResponseModel> {
        if (req.user.id) {
            const user = await this.userService.findById(req.user.id);
            if (user) {
                return {
                    displayName: user.displayName,
                };
            }
        }

        return null;
    }

    @Get('google')
    @UseGuards(AuthGuard('google'))
    googleLogin() {
        // initiates the Google OAuth2 login flow
    }

    @Get('google/callback')
    @UseGuards(AuthGuard('google'))
    googleLoginCallback(@Req() req, @Res() res) {
        const baseUrl = this.config.get('CLIENT_BASE_URL') || '';
        const redirectUrl = baseUrl + '/';

        if (req.user.jwt) {
            res.redirect(redirectUrl + '#loggedin?jwt=' + req.user.jwt);
        } else {
            res.redirect(redirectUrl + 'fail');
        }
    }
}
