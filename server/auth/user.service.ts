import { Injectable } from '@nestjs/common';
import { User, IUser } from './user.model';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';

@Injectable()
export class UserService {
    constructor(@InjectModel(User) private readonly userModel: ModelType<User>) {
    }

    async create(user: IUser): Promise<User> {
        const createdUser = new this.userModel(user);
        return await createdUser.save();
    }

    async updateAccessToken(providerId: string, accessToken: string): Promise<User> {
        return await this.userModel.updateOne({ providerId }, { $set: { accessToken } }).exec();
    }

    async findByProviderId(providerId: string): Promise<User> {
        return await this.userModel.findOne({ providerId }).exec();
    }

    async findById(id: string): Promise<User> {
        return await this.userModel.findOne({ _id: id }).exec();
    }
}
