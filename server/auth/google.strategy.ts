import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { ConfigService } from '../config/config.service';
import { AuthService } from './auth.service';
import { IUser } from './user.model';
import { access } from 'fs';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
    constructor(private readonly config: ConfigService, private readonly authService: AuthService) {
        super({
            clientID: config.get('GOOGLE_CLIENT_ID'),
            clientSecret: config.get('GOOGLE_CLIENT_SECRET'),
            callbackURL: config.get('HOST_URL') + '/auth/google/callback',
            passReqToCallback: true,
            scope: [
                'profile',
                'https://www.googleapis.com/auth/userinfo.email',
                'https://www.googleapis.com/auth/contacts.readonly'],
        });
    }

    // tslint:disable-next-line: ban-types
    async validate(request: any, accessToken: string, refreshToken: string, profile, done: Function) {
        const externalUser: IUser = {
            providerId: profile.id,
            displayName: profile.displayName,
            email: profile.emails[0].value,
            accessToken,
        };

        const jwt: string = await this.authService.validateOAuthLogin(externalUser);
        const user = { jwt };

        done(null, user);
    }
}
