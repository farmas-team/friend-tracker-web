import { Module, MiddlewareConsumer } from '@nestjs/common';
import { ServeStaticMiddleware } from '@nest-middlewares/serve-static';
import { FriendsModule } from './friends/friends.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { DatabaseModule } from './database/database.module';
import { ContactsModule } from './contacts/contacts.module';

@Module({
  imports: [
    FriendsModule,
    ContactsModule,
    AuthModule,
    ConfigModule,
    DatabaseModule,
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    ServeStaticMiddleware.configure('client/dist');

    consumer.apply(ServeStaticMiddleware)
      .exclude('auth')
      .exclude('api')
      .forRoutes('/');
  }
}
